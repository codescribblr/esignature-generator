<?php 
/*  Instructions for the E-signature Generator
1. This file and functions.php should be present in the same directory.
2. Create your html email signature in "esig.html" and place it into the same directory.
3. Individualize the index.php file:
  a. Comment out any $fields below ($image_link, $title, etc.) that are unnecessary for this client.
  b. Add the company name to the $company_name variable below.
4. In your esig.html file, add the appropriate fields to the html in php tags (ex: $name). It will be included in this file's DOM.
5. Direct clients to index.php. 
 */
// 
// Initiate file setup and functions
require_once("functions.php"); 
$company_name = "Benefits in a Card"; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>E-Signatures for <?php echo $company_name?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
</head>
<body>
<div class="container-fluid" style="padding-bottom:100px;">
<div class="page-header">
	<h1 style="color:#888;">E-Signature Generator</h1>
	<?php if($msg_id):?>
	<div class="alert alert-<?php echo ($messages[$msg_id]['type']=='error') ? "danger" : "success";?> alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  <?php echo $messages[$msg_id]['text'];?>
	</div>
	<?php endif;?>
</div>
<?php
// $_POST processing
if($_POST && !empty($_POST)) { // Form was submitted
	if(!empty($_POST)) { 
		
		// storing fields in variables
		$old_name = $_POST['old_name'];
		$name = $_POST["name"];
		$title = $_POST["title"];
		$office = $_POST["office_number"];
		$cell = $_POST["cell_number"];
		$fax = $_POST["fax_number"];
		// $image_link = $_POST["image_link"];
		$template = $_POST["template"];
		
		// $_POST as simple array for edit form
		$fields = array('name'=>$name, 'title'=>$title, 'office_number'=>$office, 'cell_number'=>$cell, 'fax_number'=>$fax, 'image_link'=>$image_link, 'template'=>$template);		

		if(isset($_POST["submit"])) { // new $_POST was submitted
			if(empty($json)) { // in case esig.json is empty
				$json = array($fields);
				if(file_put_contents($file, json_encode($json))){
					$msg_id = 0;
				} else {
					$msg_id = 3;
				}
			} else { // if there is more than one entry
				$json[] = $fields;
				if(file_put_contents($file, json_encode($json))){
					$msg_id = 0;
				} else {
					$msg_id = 3;
				}
			}
		} elseif($_POST["edit"]) { // edit $_POST was submitted
			$json[find_id_by_name($json, $old_name)] = $fields;
			if(file_put_contents($file, json_encode($json))){
				$msg_id = 1;
			} else {
				$msg_id = 3;
			}
		} else {
			safe_redirect("index.php?msg_id=".$msg_id);
		}
		$redirect = "index.php?msg_id=".$msg_id."&name=";
		$redirect .= $name;
		safe_redirect($redirect, $exit);
	}	else {
		safe_redirect("index.php");
	} 
} 

// $_GET requests below
else {
	if(isset($_GET["new"])) { // New client form
?> 
	<div class="col-sm-4">
		<div class="row">
			<h2>Create E-Signature</h2>
			<p>Please type in your information exactly as you'd like it to appear on your e-signature.</p>
		</div>
		<form class="row" action="index.php" method="post">
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" class="form-control" name="name" value=""/>
			</div>
			<div class="form-group">
				<label for="title">Title</label>
				<input type="text" class="form-control" name="title" value=""/>
			</div>
			<div class="form-group">
				<label for="office_number">Office Phone Number</label>
				<input type="text" class="form-control" name="office_number" value=""/>
			</div>
			<div class="form-group">
				<label for="cell_number">Cell Phone Number</label>
				<input type="text" class="form-control" name="cell_number" value=""/>
			</div>
			<div class="form-group">
				<label for="fax_number">Fax Number</label>
				<input type="text" class="form-control" name="fax_number" value="" />
			</div>
			<!-- <div class="form-group">
				<label for="image_link">Image Link</label>
				<input type="text" class="form-control" name="image_link" value=""/>
			</div> -->
			<div class="radio">
				<label>
					<input type="radio" name="template" id="template" value="template-1.html" checked>
					Template 1
				</label>
			</div>
			<div class="radio">
				<label>
					<input type="radio" name="template" id="template" value="template-2.html">
					Template 2
				</label>
			</div>
			<button class="btn btn-default" type="submit" name="submit" />Submit</button>
			<p style="display:inline">&nbsp;|&nbsp;<a href="index.php">Return to List of E-Signatures</a></p>
		</form>
	</div>
<?php
	// Process delete entry
	} elseif(isset($_GET["del"]) && has_presence($_GET["del"])) {
		$name = $_GET["del"];
		$json = delete_by_name($json, $name);
		if(file_put_contents($file, json_encode($json))){
			$msg_id = 2;
		} else {
			$msg_id = 4;
		}
		$redirect = "index.php?msg_id=".$msg_id;
		safe_redirect($redirect);
	}
	// Process edit entry
	elseif(isset($_GET["edit"]) && has_presence($_GET["edit"])) {
		$id = find_id_by_name($json, $_GET["edit"]);
		$fields = $json[$id];
		$old_name = $fields['old_name'];
		$name = $fields["name"];
		$title = $fields["title"];
		$office_number = $fields["office_number"];
		$cell_number = $fields["cell_number"];
		$fax_number = $fields["fax_number"];
		// $image_link = $fields["image_link"];
		$template = $fields['template'];
		
	// Edit esig form	
?> 
	<div class="col-sm-4">
		<div class="row">
			<h2>Edit: <?php echo my_urldecode($name)?></h2>
			<p>Please type in your information exactly as you'd like it to appear on your e-signature.</p>
		</div>
		<form class="row" action="index.php" method="post">		
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" class="form-control" name="name" value="<?php echo my_urldecode($name)?>" />
			</div>
			<div class="form-group">
				<label for="title">Title</label>
				<input type="text" class="form-control" name="title" value="<?php echo my_urldecode($title)?>" />
			</div>
			<div class="form-group">
				<label for="office_number">Office Phone Number</label>
				<input type="text" class="form-control" name="office_number" value="<?php echo my_urldecode($office_number)?>"/>
			</div>
			<div class="form-group">
				<label for="cell_number">Cell Phone Number</label>
				<input type="text" class="form-control" name="cell_number" value="<?php echo my_urldecode($cell_number)?>" />
			</div>
			<div class="form-group">
				<label for="fax_number">Fax Number</label>
				<input type="text" class="form-control" name="fax_number" value="<?php echo my_urldecode($fax_number)?>" />
			</div>
			<!-- <div class="form-group">
				<label for="image_link">Image Link</label>
				<input type="text" class="form-control" name="image_link" value="<?php echo $image_link ?>" />
			</div> -->
			<div class="radio">
				<label>
					<input type="radio" name="template" id="template" value="template-1.html" <?php echo ($template=='template-1.html') ? "checked" : "";?>>
					Template 1
				</label>
			</div>
			<div class="radio">
				<label>
					<input type="radio" name="template" id="template" value="template-2.html" <?php echo ($template=='template-2.html') ? "checked" : "";?>>
					Template 2
				</label>
			</div>
			<input type="hidden" name="old_name" value="<?php echo $name; ?>" />
			<input type="submit" class="btn btn-default" name="edit" value="Submit" />
			<p style="display:inline">&nbsp;|&nbsp;<a href="index.php?name=<?php echo my_urlencode($name);?>">Return to your E-Signature</a></p>
		</form>
	</div>
<?php	
	} elseif(isset($_GET["name"])) {
		echo "<div class=\"col-sm-6\">";
		if(!empty($json)) {
			$id = find_id_by_name($json, $_GET["name"]);
			$fields = $json[$id];
			$name = $fields["name"];
			$title = $fields["title"];
			$office_number = $fields["office_number"];
			$cell_number = $fields["cell_number"];
			$fax_number = $fields["fax_number"];
			// $image_link = $fields["image_link"];
			$template = $fields["template"];
			?>

			<div class="row">
				<h2><?php echo $name;?></h2>
				<?php include("templates/".$template); // Display HTML Email Signature ?>
			</div>

			<div class="row" style="margin-top:20px;">
				<nav>
					<ul class="nav nav-pills">
						<li role="presentation">
							<a href=# class="click" data-clipboard-action="copy" data-clipboard-target="#selectme" title="Copied to clipboard!" data-trigger="manual">Copy Esig to Clipboard</a>
						</li>
						<li role="presentation">
							<a href="index.php?edit=<?php echo my_urlencode($name);?>">Edit</a>
						</li>
						<li>
							<a href="index.php?del=<?php echo my_urlencode($name);?>" onclick="return confirm('Are you sure?')">Delete</a><br /><br />
						</li>
					</ul>
				</nav>
			</div>

			<div class="row">
				<h3>Good to go?</h3>
				<p>If you're satisfied with your E-signature content, click <strong>Copy Esig to Clipboard</strong> to copy your e-signature, then paste into your email program. Or click <strong>edit</strong> if you see something you need to change.</p>
			<a href="index.php">Return to List of E-Signatures</a>
			</div>
		</div>
		<?php
				
		} else {
			safe_redirect("index.php?new");
		}
	} else {  // By default, list all entries for this client
		?>
			<div class="col-sm-4">
				<h2 style="color:#303240;"><?php echo $company_name;?></h2>
				<?php if(!$json):?>
				<h4>No Esignatures Yet!</h4>
				<?php else:?>
					<ul class="nav nav-pills nav-stacked">
					<?php foreach($json as $array) :?>
					<?php if(!$array) continue;?>
						<li>
							<a href="index.php?name=<?php echo $array['name'];?>"><?php echo $array["name"];?></a>
						</li>
					<?php endforeach; ?>
					</ul>
				<?php endif;?>
				<br />
				<a class="btn btn-primary" href="index.php?new">Create a New E-Signature</a>
				<br />
				<br />
			</div>
			<?php 
		}
}

?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="_assets/js/functions.js"></script>
</body>
</html>