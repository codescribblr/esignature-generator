<?php 
// File setup
$file = 'esig.json';
$json = array();
if(file_exists($file)) { // Checks for existence of esig.json file; if false, creates it
	$json = json_decode(file_get_contents($file), true);
	$json = $json ? array_filter($json) : $json;
} else {
	file_put_contents($file, "");
	redirect_to("index.php?new");
}
$msg_id = (isset($_GET['msg_id']) && !empty($_GET['msg_id'])) ? $_GET['msg_id'] : false;
$messages = array();
$messages[0] = array('type'=>'success', 'text'=>"Esig Created Successfully!");
$messages[1] = array('type'=>'success', 'text'=>"Esig Edited Successfully!");
$messages[2] = array('type'=>'success', 'text'=>"Esig Deleted Successfully!");
$messages[3] = array('type'=>'error', 'text'=>"There was an error saving your Esig. Please try again. If error persists, contact <a href='mailto:websupport@createlaunchlead.com?subject=Esig save error'>support</a>.");
$messages[4] = array('type'=>'error', 'text'=>"There was an error deleting your Esig. Please try again. If error persists, contact <a href='mailto:websupport@createlaunchlead.com?subject=Esig delete error'>support</a>.");

function redirect_to($new_location) {
	header("Location: " . $new_location);
	exit;
}

function safe_redirect($url, $exit = TRUE) {
		try {
			// Only use the header redirection if headers are not already sent
			if (!headers_sent()) {
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: ' . $url);
				// Optional workaround for an IE bug (thanks Olav)
				header("Connection: close");
			}
			// HTML/JS Fallback:
			// If the header redirection did not work, try to use various other methods
			print '<html>';
			print '<head><title>Redirecting you...</title>';
			print '<meta http-equiv="Refresh" content="0;url=' . $url . '" />';
			print '</head>';
			print '<body onload="location.replace(\'' . $url . '\')">';
			// If the javascript and meta redirect did not work, 
			// the user can still click this link
			print 'You should be redirected to this URL:<br />';
			print "<a href='$url'>$url</a><br /><br />";
			print 'If you are not, please click on the link above.<br />';
			print '</body>';
			print '</html>';
			// Stop the script here (optional)
			if ($exit) {
				exit;
			}
		} catch (Exception $err) {
			return $err->getMessage();
		}
	}
function has_presence($value) {
	return isset($value) && $value !== "";
}	

function my_urlencode($string) {
	strtolower(str_replace(" ", "_", $string));
	return $string;
}

function my_urldecode($string) {
	ucwords(str_replace("_", " ", urldecode($string)));
	return $string;
}

// Dev functions
function display_array($array) {
	echo "<pre>";
	print_r($array);
	echo "</pre>";
}

function find_id_by_name($json, $name) {
	$name = strtolower(str_replace("_", " ", urldecode($name)));
	foreach($json as $id=>$data) {	
		if (strtolower($data["name"]) == $name) {
			return $id;
		}
	}
}

function delete_by_name($json, $get_name) {
	$name = strtolower(str_replace("_", " ", urldecode($get_name)));
	foreach($json as $id=>$data) {
		if(strtolower($data["name"]) == $name) {
			unset($json[$id]);
		} else {
			continue;
		}
		return $json;
	}
}